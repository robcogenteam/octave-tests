
%% Test of the C++ inverse dynamics generated by RobCoGen, for floating-base
%% robots under the assumption of a fully actuated base.
%%
%% Usage: [ME ROY] = testcpp_id_fb_actuated(MODEL, EXE)
%%
%% MODEL is the robot model (a data structure) in Roy's format.
%%
%% EXE is the full path of the program running the C++ RobCoGen inverse
%% dynamics for the same robot. The program must take the following command
%% line arguments:
%%   - status, velocity and desired acceleration for each joint
%%   - coordinates of the spatial velocity and desired acceleration of the base
%%     (in base coordinates)
%%   - the 3D vector with the gravity acceleration in base coordinates
%%   - (optional) the full path with a text file with external forces
%%
%% That is, the program must be executable with something like the following:
%%    "<exe> <q'> <qd'> <qdd'> <base_v'> <base_a'> <g'> <path to fext>"
%% The program is expected to print to stdout the results, i.e. the spatial
%% force to be applied to the base (the "base wrench") and the joint space
%% force vector. These two vectors are expected to be printed one after the
%% other, one coordinate per line.
%%
%% See readme.md for more information and general documentation on the inputs
%% of this function.
%%
%% Author: Marco Frigerio
%
%%% This file is part of the Octave test functions for RobCoGen.
%%% Copyright © 2016 2017, Marco Frigerio (`marco.frigerio@iit.it`)
%%% See the LICENSE file for more information.

function [q qd qdd base_v base_a sJq sJqd roy] = testcpp_id_fb_actuated(roy_model, exe)

dof = length(roy_model.jtype);

q   = rand(dof,1);
qd  = rand(dof,1);
qdd = rand(dof,1);

base_v = rand(6,1);
base_a = rand(6,1);

q(1:6) = zeros(6,1); % FB status to zero
% q   = zeros(dof,1);
% qd  = zeros(dof,1);
% qdd = zeros(dof,1);

% base_v = zeros(6,1);
% base_a = zeros(6,1);


%
% Random external forces
%
% (5 bodies are dummy links for the floating base virtual joint)
%
fext = cell(1, roy_model.NB);
for i = 1 : 5
  fext{i} = zeros(6, 1);
end
for i = 6 : length(fext)
  fext{i} = rand(6, 1);
  %fext{i} = zeros(6, 1);
end
% Write a file with the external forces. Ignore the first 5 elements,
%  because they refer to the dummy links of the floating base.
fid = fopen('fext.txt', 'w');
for i = 6 : length(fext)
  fprintf(fid, "%f %f %f %f %f %f\n", fext{i});
end
fclose(fid);
fextFile = [pwd '/fext.txt']; % full absolute path of the external forces file

% The gravitational acceleration in base coordinates
gravity = gravityFBCoordinates(q(4:6));

%
% Roy's routine for fixed base robots is basically the same as RobCoGen inverse
% dynamics for fully actuated base. The only difference is that Roy uses the
% virtual 6 dof joint, whereas RobCoGen uses spatial vectors in base coordinates.
% Therefore, converting some of the inputs is necessary, before running and
% comparing the two implementations.

% Velocity and acceleration of the virtual 6-DoF-joint.
% The function also computes the Jacobian that relates the virtual joint
% velocity to the base velocity.
[qd(1:6,1) qdd(1:6,1) sJq sJqd] = bodyToFBJointMotion(q(1:6), base_v, base_a);

% Convert the spatial external force on the base into a virtual 6-DoF-joint
% force vector
ffbj = sJq' * fext{6};

% Now, I still need to set properly the spatial external forces acting on the
% virtual links of the virtual floating base joint. These will have only one
% non-zero coordinate depending on the type of the joint
for i = 1:6
  [dummy, S] = jcalc( roy_model.jtype{i}, q(i) );
  fext{i} = S * ffbj(i);
end

%
% Run Roy's matlab inverse dynamics
%
bigtau     = ID(roy_model, q, qd, qdd, fext);
roy.tau_bb = bigtau(1:6);    % floating-base joint force vector
roy.tau    = bigtau(7:end);  % actuated-joints force vector

%
% Run the C++ RobCoGen implementation
%
[foo output] = system([exe ' ' num2str([q(7:end)', qd(7:end)', qdd(7:end)', base_v', base_a', gravity']) ' ' fextFile] );
output    = str2num(output);
me.base_w = output(1:6);   % base wrench
me.tau    = output(7:end); % actuated-joints force vector

%
% Compare
%
err.base_w = roy.tau_bb - sJq' * me.base_w;
err.tau    = roy.tau    - me.tau;

err.base_w( abs(err.base_w) < 1E-5 ) = 0;
err.tau   ( abs(err.tau   ) < 1E-5 ) = 0;

display([roy.tau_bb, sJq' * me.base_w, err.base_w]);
display([roy.tau, me.tau, err.tau]);


