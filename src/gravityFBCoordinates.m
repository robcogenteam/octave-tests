
%%
%% Usage: A = gravityFBCoordinates(Q, FBJ_SPECS)
%%
%% Computes the gravitational acceleration in body coordinates, given
%% the orientation of the body in space.
%%
%% Q is a vector with the three Euler angles that describe the body orientation
%% - i.e. the status variable of a virtual 3-dof joint which we imagine between
%% the ground and the body.
%% FBJ_SPECS is the specification of the virtual 3-dof joint, as a cell array of
%% three strings (optional parameter). The default value is {'Rx', 'Ry', 'Rz'},
%% which corresponds to three rotations about x,y,z (successive rotations); see
%% the documentation in Roy's code 'spatial_v2'.
%%
%% The function returns the 3D gravity acceleration A in body coordinates.
%%
%% This function depends on Roy's 'spatial_v2', namely the 'jcalc' function.
%%
%% Author: Marco Frigerio
%
%%% This file is part of the Octave test functions for RobCoGen.
%%% Copyright © 2016 2017, Marco Frigerio (`marco.frigerio@iit.it`)
%%% See the LICENSE file for more information.

function a_grav = gravityFBCoordinates(q, fbjoint_specs)

if nargin < 2
    fbjoint_specs = {'Rx', 'Ry', 'Rz'};
end

X = eye(6);
for i = 1 : 3
    [ XJ, S] = jcalc( fbjoint_specs{i}, q(i) );
    X = XJ * X;
end

a_grav = X(4:6,end) * -9.81;

