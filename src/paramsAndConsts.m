function [params consts] = paramsAndConsts(robcogenStructs)

if( isfield(robcogenStructs, 'kk') )
  consts = robcogenStructs.kk;
else
  consts = [];
end

if( isfield(robcogenStructs, 'pp') )
  params = robcogenStructs.pp;
else
  params = [];
end
